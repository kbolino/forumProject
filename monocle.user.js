// ==UserScript==
// @name        Monocle
// @namespace   Glibertarians
// @include     *glibertarians.com/2*
// @version     1.18
// @grant       letMeIn
// ==/UserScript==


//Feature Variables
var fullCommentReply = false;
var inputFormatterLoaded = false;

//make choice persistent for hide/show old threads
var hideThreadsStorageTag = "hideOldThreads";
var expandMainColStorageTag = "expandMainColumn";
var timeZoneStorageTag = "chgTimeZone";
var compressViewStorageTag = "compressView";
var replyClickStorageTag = "replyClick";
var dynamicLoadStorageTag = "dynamicLoad";
var floatCommStorageTag = "floatCommentBox";

if (localStorage.getItem(floatCommStorageTag) == 1){
  var inputFormatter = "custom"; //"tinyMCE";
}
else{
  var inputFormatter = "custom"; //"tinyMCE";
}


if ( typeof(localStorage.getItem(hideThreadsStorageTag)) == 'undefined' ) {
    localStorage.setItem(hideThreadsStorageTag, 0);
}
if ( typeof(localStorage.getItem(expandMainColStorageTag)) == 'undefined'){
    localStorage.setItem(expandMainColStorageTag, 0);
}
if ( typeof(localStorage.getItem(timeZoneStorageTag)) == 'undefined'){
    localStorage.setItem(timeZoneStorageTag, 0);
}
if ( typeof(localStorage.getItem(compressViewStorageTag)) == 'undefined'){
    localStorage.setItem(compressViewStorageTag, 0);
}		
if ( typeof(localStorage.getItem(replyClickStorageTag)) == 'undefined'){
    localStorage.setItem(replyClickStorageTag, 0);
}		
if ( typeof(localStorage.getItem(dynamicLoadStorageTag)) == 'undefined'){
    localStorage.setItem(dynamicLoadStorageTag, 0);
}
if ( typeof(localStorage.getItem(floatCommStorageTag)) == 'undefined'){
    localStorage.setItem(floatCommStorageTag, 0);
}		

//HTML tagging
function customFormatter() {
    document.getElementById('comment').value="";
    var tags = [ "a=link", "strong=bold", "strike=strikethrough", "em=italics", "blockquote=blockquote"];
    var sfc = document.createElement("span");
    for ( tag in tags ) {
        var tagname=tags[tag].split('=')[0];
        var tagdesc=tags[tag].split('=')[1];
        var thisTag=document.createElement('button');
        thisTag.id=tagname + "-tag";
        thisTag.setAttribute('title', tagdesc + ' text');
        if ( tagname == "a" ) {
            thisTag.onclick = function(but){ 
              var form=but.target.parentElement.parentElement.parentElement;
              doAnchorDialog(form.getElementsByTagName('textarea')[0]); 
              return false; 
            };
        }
        else {
            thisTag.onclick = function(but) { 
              console.log("click event = ",but);
              var form=but.target.parentElement.parentElement.parentElement;
              myFormatText(but.target.id.replace(/-tag/,""),form.getElementsByTagName('textarea')[0]); 
              return false; 
            };
        }
        thisTag.innerHTML=tagdesc;
        sfc.appendChild(thisTag);
    }
    var sa=document.getElementById('comment');
    sa.parentNode.insertBefore(sfc, sa);
}

function myFormatText(tag,ta) {
    var reptext="";
    var startPos = ta.selectionStart;
    var endPos = ta.selectionEnd;
    if ( tag == "a" ) {
        reptext="<a href=" + ta.value.substring(startPos, endPos) + 'target="_blank"' + ">" + ta.value.substring(startPos, endPos) + "</a>";
    }
    else if ( document.getSelection().anchorNode != null && ! document.getSelection().anchorNode.contains(ta) && document.getSelection().toString().length > 0 ) {
      console.log('select-o-matic charged!');
      var tmpdiv = document.createElement("div");
      var content=document.getElementsByClassName('post')[0];
      var comment=document.getElementById('comment-wrap');
      var cite=document.getElementsByClassName('fn')[0].innerText;
      var pre="";
      var post="";
      var citeTime="";
      if (comment.contains(document.getSelection().anchorNode)) {
          var li;
          var notli=document.getSelection().anchorNode;
          for (var a=0; a < 10; a++ ) {
              li=notli.parentElement;
              if (li.tagName == 'ARTICLE' ) {
                  break;
              }
              else {
                  notli=li;
              }
          }
          citeTime=li.getElementsByClassName('comment_date')[0].innerText.replace("on ","");
          cite=li.getElementsByClassName('fn')[0].innerText.replace(/<script(?:.|\s)*\/script>/m, "");
      }
      if ( tag == "cite" ) {
          pre = "<cite>" + cite + " said";
          if ( citeTime.length > 0 ) {
              pre += " @" + citeTime;
          }
          pre += ": </cite><blockquote><P>";
          post = "</P></blockquote>";
      }
      else {
          pre="<" + tag + ">";
          post="</" + tag + ">";
      }
      var sel = window.getSelection();
      for (var i = 0, len = sel.rangeCount; i < len; ++i) {
        tmpdiv.appendChild(sel.getRangeAt(i).cloneContents());
      }
      reptext = reptext.concat(tmpdiv.innerHTML).replace(/<!--[^>]*>/,"").replace(/<\/?div[^>]*>/,"").replace(/<span class="new-text">[^>]*>/,"").replace(/<\/?span[^>]*>/,"");
      reptext = pre + reptext + post;
    }
    else {
        reptext="<" + tag + ">" + ta.value.substring(startPos, endPos) + "</" + tag + ">";
    }
    replaceWhereWithWhat(ta,startPos,endPos,reptext);
}

function replaceWhereWithWhat(ta,startPos,endPos,reptext) {
    if (ta.value == "COMMENT"){
      killPreloadedCommentText();
    }
    ta.value=ta.value.substring(0,startPos) + reptext + ta.value.substring(endPos,ta.value.length);
}

function doAnchorDialog(ta) {
    ta=document.getElementsByTagName('textarea')[0];

    var startPos = ta.selectionStart;
    var endPos = ta.selectionEnd;
    
    mydiv=document.createElement('div');
    mydiv.setAttribute('role','dialog');
    mydiv.setAttribute('class','anchor-floater');
    mydiv.setAttribute('tabindex',0);
    mydiv.setAttribute('style','left: 480px; top: 324px;');
    mydiv.id='anchor-dialog';

    var mybut=document.createElement('button');
    mybut.id='anchor-closer';
    mybut.setAttribute('title', 'close it');
    mybut.onclick=function() { document.getElementById('anchor-dialog').remove(); return false; };
    mybut.innerHTML='X';
    mydiv.appendChild(mybut);

    var anchorName=document.createElement('input');
    anchorName.id='anchorName';
    anchorName.value=ta.value.substring(startPos, endPos);
    var anchorNameLab=document.createElement('label');
    anchorNameLab.innerHTML='Display Text';
    mydiv.appendChild(anchorNameLab);
    mydiv.appendChild(anchorName);

    var anchorUrl=document.createElement('input');
    anchorUrl.id='anchorUrl';
    anchorUrl.tabIndex=0;
    var tmpval=ta.value.substring(startPos, endPos);
    if ( tmpval.length > 0 && tmpval.substring(0,4) != "http" ) {
        tmpval="http://" + tmpval;
    }
    anchorUrl.value=tmpval;
    var anchorUrlLab = document.createElement('label');
    anchorUrlLab.innerHTML='Url';
    mydiv.appendChild(anchorUrlLab);
    mydiv.appendChild(anchorUrl);
    
    var titleName=document.createElement('input');
    titleName.id='titleName';
    titleName.value=ta.value.substring(startPos, endPos);
    var titleNameLab=document.createElement('label');
    titleNameLab.innerHTML='Hover Text';
    mydiv.appendChild(titleNameLab);
    mydiv.appendChild(titleName);

    var mysubbut=document.createElement('button');
    mysubbut.id='anchor-submit';
    mysubbut.onclick=function() { 
        var tmpval2=anchorUrl.value;
        if ( tmpval2.length > 0 && tmpval2.substring(0,4) != "http" ) {
            tmpval2="http://" + tmpval2;
        }
        reptext='<a href="' + tmpval2 + '" ';
        reptext += (titleName.value) ? 'title="' + titleName.value + '" ' : '';
        reptext += ' target="_blank" ';
        reptext += '>' + anchorName.value + '</a>';
        replaceWhereWithWhat(ta,startPos,endPos,reptext);
        document.getElementById('anchor-dialog').remove(); 
        var cc=ta.parentElement.getElementsByClassName('characterCount')[0];
        updateNumCharsInElement(ta.value,cc,maxCommentLength);
        return false; 
    };
    mysubbut.innerHTML='submit';
    mydiv.appendChild(mysubbut);

    ta.parentElement.appendChild(mydiv);
    anchorName.focus();
}

//WYSIWYG commenting
function addTinyMCE(){
    var myScript = document.createElement('script');
    document.head.appendChild(myScript);
    //myScript.onload = function() { loadTinyMCEin(); } ;    
    myScript.id = "myTinyMCE";
    myScript.src = "https://cdn.tinymce.com/4/tinymce.min.js";    
    var initScript = document.createElement("script");
    initScript.type = "text/javascript";
    initScript.id = "tinyMCEinit";
    initScript.innerHTML = "window.eval(tinymce.init({selector: 'textarea#comment', toolbar: 'bold italic strikethrough | blockquote link | charmap', browser_spellcheck: true, plugins: 'autolink link charmap', menubar: false}));";
    myScript.onload = function () {document.querySelector("#comment").value = ""; document.head.appendChild(initScript);};    
}

function resetTinyMCE(){
  window.eval(tinymce.remove());
  var loadScript = document.getElementById("myTinyMCE");
  loadScript.parentNode.removeChild(loadScript);
  var iniScript = document.getElementById("tinyMCEinit");
  iniScript.parentNode.removeChild(iniScript);
  addTinyMCE();
}

//function to remove the sidebar
function expandMainColumn(){
  var sideCol = document.querySelector(".et_pb_extra_column_sidebar");
  sideCol.parentNode.removeChild(sideCol);
  var mainCol = document.querySelector(".et_pb_extra_column_main");
  mainCol.className = "";
}

//functions to set the local time zone on all comments
function setTimeZone(){
  var comments = document.querySelectorAll(".comment_date");
  for (var i = 0; i < comments.length; i++){
    adjustTime(comments[i]);
  }
}

function isDstActive(){
  // based on stackoverflow.com/q/11887934
  // this is faulty because it relies on the user's DST transition
  // happening on the same day as the server's DST transition
  var now = new Date();
  var jan = new Date(now.getFullYear(), 0, 1);
  return jan.getTimezoneOffset() != now.getTimezoneOffset();
}

function adjustTime(commentDate){ 
  var dateTokens = commentDate.innerHTML.split(" ");
  var tzOffset = isDstActive() ? "GMT-0500" : "GMT-0600";
  var dateObj = new Date(dateTokens[1] + " " + dateTokens[2] + " " + dateTokens[3] + " " + dateTokens[5] + " " + dateTokens[6] + " " + tzOffset);
  var options = {
    year: "numeric", month: "long",
    day: "numeric", hour: "2-digit", minute: "2-digit"
  };
  commentDate.innerHTML = "on " + dateObj.toLocaleTimeString("en-us",options);
}

//create footer toolbar
var unreadCount = 0;
var oldThreadVisible = true;
function createFooter(){
  var toolbar = document.createElement("div");
  toolbar.id = "monocle_tb_container";

  //visible portion
  var activebar = document.createElement("div");
  activebar.id = "monocle_toolbar";
  activebar.className = "visible";
  //scrolls to unread comments
  var nextUnreadButton = document.createElement("input");
  nextUnreadButton.id = "unread_button";
  nextUnreadButton.type = "button";
  nextUnreadButton.className = "mono_button";
  nextUnreadButton.value = document.querySelectorAll(".new-comment").length + " Unread Comments";
  if (document.querySelectorAll(".new-comment").length == 1){
    nextUnreadButton.value = document.querySelectorAll(".new-comment").length + " Unread Comment";
  }
  nextUnreadButton.addEventListener("click",seekNextUnread);
  activebar.appendChild(nextUnreadButton);
  //toggles visibility of old threads
  var oldThreadButton = document.createElement("input");
  oldThreadButton.id = "old_thread_button";
  oldThreadButton.type = "button";
  oldThreadButton.className = "mono_button";
  oldThreadButton.value="Hide Old Threads";
  oldThreadButton.addEventListener("click", toggleOldThreads);
  activebar.appendChild(oldThreadButton);
  //moves view to top
  var toTopButton = document.createElement("input");
  toTopButton.id = "to_top_button";
  toTopButton.type = "button";
  toTopButton.className = "mono_button";
  toTopButton.value="Top of Comments";
  toTopButton.addEventListener("click", navigateToTop);
  activebar.appendChild(toTopButton);
  //clears new comments
  var markReadButton = document.createElement("input");
  markReadButton.id = "mark_read_button";
  markReadButton.type = "button";
  markReadButton.className = "mono_button";
  markReadButton.value="Mark Comments as Read";
  markReadButton.addEventListener("click", markAsRead);
  activebar.appendChild(markReadButton);
  //toggles visibility of toolbar
  var toggleButton = document.createElement("input");
  toggleButton.id = "toggle_button";
  toggleButton.type = "button";
  toggleButton.className = "mono_button";
  toggleButton.value = "v";
  toggleButton.addEventListener("click",toggleToolbar);
  toolbar.appendChild(toggleButton);
  toolbar.appendChild(activebar);
  document.getElementById("footer").appendChild(toolbar);
  setShowThreads(oldThreadButton); //sets button value
  //create options button and bar
  createOptionsBar();
  var optionsButton = document.createElement('input');
  optionsButton.id = 'options_button';
  optionsButton.type ='button';
  optionsButton.className = 'mono_button';
  optionsButton.value = 'Options';  
  optionsButton.addEventListener('click', toggleOptionsBar);
  activebar.appendChild(optionsButton);
}

function createOptionsBar(){
  var toolbar = document.getElementById('monocle_tb_container');
  var visibar = document.getElementById('monocle_toolbar');
  var optionsbar = document.createElement('div');
  optionsbar.id = "options_menu";  
  optionsbar.style = "overflow-y:scroll";
  optionsbar.classList.add("mini");
  document.body.appendChild(optionsbar);
  //expand column
  var expandCollab = document.createElement('input');  
  expandCollab.type = "button";
  expandCollab.className = "option_text";
  expandCollab.id = "expand_col_button";
  expandCollab.addEventListener('click',toggleExpandedMainCol);
  if (localStorage.getItem(expandMainColStorageTag) == 1){
	  expandCollab.value ="Do Not Expand Main Column";
	  expandCollab.classList.add("selected");
  }
  else{
	expandCollab.value ="Expand Main Column";
  }
  optionsbar.appendChild(expandCollab);
  var breaker = document.createElement('br');
  optionsbar.appendChild(breaker);
  //set time zone
  var timeZonelab = document.createElement('input');
  timeZonelab.type = "button";
  timeZonelab.className = "option_text";
  timeZonelab.id = "time_zone_button";
  timeZonelab.addEventListener('click',toggleTimeZone);
  if (localStorage.getItem(timeZoneStorageTag) == 1){
    timeZonelab.value = "Set Server Time Zone";
	  timeZonelab.classList.add("selected");
  }
  else{
    timeZonelab.value = "Set Local Time Zone";
  }    
  optionsbar.appendChild(timeZonelab);
  var breaker2 = document.createElement('br');
  optionsbar.appendChild(breaker2);
  //set compressed view
  var compressViewlab = document.createElement('input');  
  compressViewlab.type = "button";
  compressViewlab.className = "option_text";
  compressViewlab.id = "compress_view_button";
  compressViewlab.addEventListener('click',toggleCompressView);
  if (localStorage.getItem(compressViewStorageTag) == 1){
    compressViewlab.value = "Expand Comments";
	  compressViewlab.classList.add("selected");
  }
  else{
    compressViewlab.value = "Compress Comments";
  }
  optionsbar.appendChild(compressViewlab);
  var breaker3 = document.createElement('br');
  optionsbar.appendChild(breaker3);
  //reply on click of comment
  var replyClicklab = document.createElement('input');  
  replyClicklab.type = "button";
  replyClicklab.className = "option_text";
  replyClicklab.id = "reply_click_button";
  replyClicklab.addEventListener('click',toggleReplyClick);
  if (localStorage.getItem(replyClickStorageTag) == 1){
    replyClicklab.value = "Reply Click on Button";
	  replyClicklab.classList.add("selected");
    var comments = document.getElementsByClassName('comment-body');
      if (localStorage.getItem(floatCommStorageTag) == 1){
       for (var i = 0; i < comments.length; i++){
         var comment = comments[i];               
         //floating reply model
         comment.addEventListener('click',replyTo);                 
       }
    }
    else {
      for (var i = 0; i < comments.length; i++){
         var comment = comments[i];      
         var replyboxes = comment.getElementsByClassName('comment-reply-link');
         if (replyboxes.length > 0){
           var replybox = replyboxes[0];   
           comment.onclick = replybox.onclick;
         } //traditional reply model
       }
    }    
  }
  else{
    replyClicklab.value = "Reply Click on Comment";
  }
  optionsbar.appendChild(replyClicklab);
  var breaker4 = document.createElement('br');
  optionsbar.appendChild(breaker4);
  //dynamically load comments
  var dynamicLoadlab = document.createElement('input');  
  dynamicLoadlab.type = "button";
  dynamicLoadlab.className = "option_text";
  dynamicLoadlab.id = "dynamic_load_button";
  dynamicLoadlab.addEventListener('click',toggleDynamicLoad);
  if (localStorage.getItem(dynamicLoadStorageTag) == 1){
    dynamicLoadlab.value = "Don't load Comments";
	  dynamicLoadlab.classList.add("selected");
    dynamicLoadInit();
  }
  else{
    dynamicLoadlab.value = "Dynamically Load Comments";    
  }
  optionsbar.appendChild(dynamicLoadlab);
  var breaker5 = document.createElement('br');
  optionsbar.appendChild(breaker5);
  //float the comment box
  var floatCommlab = document.createElement('input');  
  floatCommlab.type = "button";
  floatCommlab.className = "option_text";
  floatCommlab.id = "float_comm_button";
  floatCommlab.addEventListener('click',toggleFloatComm);
  if (localStorage.getItem(floatCommStorageTag) == 1){
    floatCommlab.value = "Don't Float Reply";
	  floatCommlab.classList.add("selected");
    floatReply();
  }
  else{
    floatCommlab.value = "Float Reply";    
  }
  optionsbar.appendChild(floatCommlab);
  var breaker6 = document.createElement('br');
  optionsbar.appendChild(breaker6);
}

function toggleOptionsBar(){
  var optionsbar = document.getElementById('options_menu');
  if (optionsbar.classList.contains("mini")){
    optionsbar.classList.remove("mini");
  }
  else{
    optionsbar.classList.add("mini");
  }
}

//add stylesheet in one place vs everywhere
//called from main, since that is only done once
function doStyleSheet(){
    var oldEle = document.getElementById("monocle-style-css");
    if (oldEle){
      oldEle.parentNode.removeChild(oldEle);
    }
    var styleEle = document.createElement('style');
    styleEle.type = 'text/css';
    styleEle.id = "monocle-style-css";
    styleEle.textContent = '#monocle_tb_container { position: fixed; z-index: 100; left: 0px; bottom: 0px; height: 60px; width: 100%; } ' +
        '#monocle_tb_container.mini { height: 30px; } ' +        
        '#monocle_tb_container input {font-size: 12px;}' + 
        '#monocle_toolbar { left:30px; bottom:0px; height: 30px; width:100%; background-color: rgba(0,0,0,0.5); } ' +
        '#monocle_tb_container.mini #monocle_toolbar { height: 0px; } ' +
        '#options_menu { position:fixed; z-index: 101; right:0px; bottom:30px; height: 400px; width:250px; background-color: rgba(0,0,0,0.5); } ' +
        '#monocle_tb_container.mini #options_menu { height: 0px; } ' +
        '#options_menu.mini { height: 0px; } ' +
        '#options_menu > input.option_text { background-image: none; padding: 1px 1px 1px 1px; font-size: small; height: 25px; width: 232px; background-color: rgba(0,0,0,0); color: white}' +
        '#options_menu > input.option_text.selected {background-color: rgba(255,255,255,0.5)}' +     
        'input.mono_button { background-image: none; padding: 1px 1px 1px 1px; height: 30px; text-shadow: #ffffff 0px 0px 2px; -webkit-font-smoothing: antialiased } ' +
        '#monocle_toolbar > #unread_button { float:left; width: 160px; background-color: rgba(0,0,0,0.9); color: white; } ' +
        '#monocle_toolbar > #mark_read_button { float:left; width: 175px; background-color: rgba(0,0,0,0.9); color: white; } ' +
        '#monocle_toolbar > #to_top_button { float:left; width: 160px; background-color: rgba(0,0,0,0.9); color: white; } ' +
        '#monocle_toolbar > #old_thread_button { float:left; width: 160px; background-color: rgba(0,0,0,0.9); color: white } ' +
        '#monocle_toolbar > #options_button { float:right; width: 160px; background-color: rgba(0,0,0,0.9); color: white } ' +  
        '#monocle_tb_container > #toggle_button { left:0px; bottom:0px; width:20px; background-color: rgba(0,0,0,0.5); }' +
        '#comment-wrap.onlyNew li.comment.no-new-comment { display: none; } ' +
		    '#respond.comment-respond.float {position: fixed; z-index: 102; padding-top: 0px; left:25px; bottom: 30px; height: 130px; border-top-left-radius: 15px; border-top-right-radius: 15px; background-color: rgba(0,0,0,0.5)}' + 
        '#respond.comment-respond.float.tinyMCE {height: 210px}' + 
        '#respond.comment-respond.float.bottom {bottom: 0px}' + 
        '#respond.comment-respond.float span {padding-left: 20px}' + 
        '#respond.comment-respond.float .comment-form-comment {margin-bottom: 5px}' +
        '#respond.comment-respond.float > #reply-title {display:none;}' +
        '#respond.comment-respond.float .logged-in-as {display:none;}' +
        '#respond.comment-respond.float #commentform {padding-bottom: 0px;}' +
        '#respond.comment-respond.float input {height: 20px; padding: 1px 1px 1px 1px; background-image: none;  background-color: rgba(0,0,0,0.9); color: white}' +
        '#respond.comment-respond.float button {height: 20px; padding: 1px 1px 1px 1px; background-image: none;  background-color: rgba(0,0,0,0.9); color: white}' +
        '#respond.comment-respond.float.tinyMCE button {height: 20px; padding: 1px 1px 1px 1px; background-image: none;  background-color: rgba(255,255,255,0.9); color: white}' +
        '#respond.comment-respond.float > textarea#comment {width: 450px; height: 200px}' +
        'input#rep_to_button.hidden {display: none;}' +
        'input#cancel_button.hidden {display: none;}';
    if ( localStorage.getItem(compressViewStorageTag) == 1 && localStorage.getItem(replyClickStorageTag) == 0 ) {
        styleEle.textContent = styleEle.textContent + ' li.comment > article.comment-body { margin-bottom: 0px; } ';
        styleEle.textContent = styleEle.textContent + ' li.comment.deepest-comment > article.comment-body { margin-bottom: 10px; } ';
    }
    if ( localStorage.getItem(replyClickStorageTag) == 1 ){
        styleEle.textContent = styleEle.textContent + 'div.comment-content > span.reply-container {display:none;}';
        styleEle.textContent = styleEle.textContent + ' li.comment > article.comment-body { margin-bottom: 15px; } ';
    }
    styleEle.textContent = styleEle.textContent + '';
    document.head.appendChild(styleEle);
}

//add new class when there is no new comments below, making the toggling a matter of changing the class of the parent object
function parseComments(){  
  var commentList = document.querySelector(".commentlist");
  var comments = commentList.querySelectorAll("li");
  for(var i = 0; i < comments.length; i++){
    var newSibling = false;
    if (!comments[i].classList.contains("depth-1")){
      var siblings = comments[i].parentNode.children;
      for (var j = 0; j < siblings.length; j++){
         if (siblings[j].classList.contains("new-comment")){
           newSibling = true;
           break;
         }
      }
    }
    else if (comments[i].classList.contains("new-comment")){
      newSibling = true;
    }
    if ( comments[i].getElementsByClassName("new-comment").length == 0 && !newSibling) {
        comments[i].classList.add("no-new-comment");
    }
    if (comments[i].getElementsByClassName("reply-container").length==0){
      comments[i].classList.add("deepest-comment");
    }
  }
}

function navigateToTop(){
  document.getElementById("comments").scrollIntoView();
  window.scrollBy(0,-50);
}

function markAsRead(){
  try{
  var newcomm = document.querySelectorAll(".new-comment");
  for (var i = 0; i < newcomm.length; i++){
    newcomm[i].classList.remove('new-comment');
  }
  var oldthr = document.querySelectorAll('.no-new-comment');
    for (var j = 0; j < oldthr.length; j++){
      oldthr[j].classList.remove('no-new-comment');
    }
  parseComments();
  var nextUnreadButton = document.getElementById("unread_button");
  nextUnreadButton.value = document.querySelectorAll(".new-comment").length + " Unread Comments";
  if (document.querySelectorAll(".new-comment").length == 1){
    nextUnreadButton.value = document.querySelectorAll(".new-comment").length + " Unread Comment";
  }  
  }
  catch(e){console.log(e.name + ": " + e.message);}
}

function toggleOldThreads(){
  if (localStorage.getItem(hideThreadsStorageTag) == 1){
    localStorage.setItem(hideThreadsStorageTag, 0);
  }
  else{
    localStorage.setItem(hideThreadsStorageTag, 1);
  }
  setShowThreads(this);
}

function toggleExpandedMainCol(){
  if (localStorage.getItem(expandMainColStorageTag) == 1){
    localStorage.setItem(expandMainColStorageTag,0);  
    document.getElementById('expand_col_button').value = "Expand Main Column";
    document.getElementById('expand_col_button').classList.remove("selected");
  }
  else{
    localStorage.setItem(expandMainColStorageTag,1);  
    document.getElementById('expand_col_button').value = "Do Not Expand Main Column";
    document.getElementById('expand_col_button').classList.add("selected");
  }
}

function toggleTimeZone(){
  if (localStorage.getItem(timeZoneStorageTag) == 1){
    localStorage.setItem(timeZoneStorageTag,0);  
    document.getElementById('time_zone_button').value = "Set Local Time Zone";
    document.getElementById('time_zone_button').classList.remove("selected");
  }
  else{
    localStorage.setItem(timeZoneStorageTag,1);  
    document.getElementById('time_zone_button').value = "Set Server Time Zone";
    document.getElementById('time_zone_button').classList.add("selected");
  }
}

function toggleCompressView(){
  if (localStorage.getItem(compressViewStorageTag) == 1){
    localStorage.setItem(compressViewStorageTag,0);  
    document.getElementById('compress_view_button').value = "Compress Comments";
    document.getElementById('compress_view_button').classList.remove("selected");
  }
  else{
    localStorage.setItem(compressViewStorageTag,1);  
    document.getElementById('compress_view_button').value = "Expand Comments";
    document.getElementById('compress_view_button').classList.add("selected");
  }
  doStyleSheet();
}

function toggleReplyClick(){
  if (localStorage.getItem(replyClickStorageTag) == 1){
    localStorage.setItem(replyClickStorageTag,0);  
    document.getElementById('reply_click_button').value = "Reply Click on Comment";
    document.getElementById('reply_click_button').classList.remove("selected");
    var comments = document.getElementsByClassName('comment-body');
    for (comment in comments){
      comment.onclick = "";
    }
  }
  else{
    localStorage.setItem(replyClickStorageTag,1);  
    document.getElementById('reply_click_button').value = "Reply Click on Button";
    document.getElementById('reply_click_button').classList.add("selected");
    var comments = document.getElementsByClassName('comment-body');
    if (localStorage.getItem(floatCommStorageTag) == 1){
       for (var i = 0; i < comments.length; i++){
         var comment = comments[i];               
         //floating reply model
         comment.addEventListener('click',replyTo);                 
       }
    }
    else {
      for (var i = 0; i < comments.length; i++){
         var comment = comments[i];      
         var replyboxes = comment.getElementsByClassName('comment-reply-link');
         if (replyboxes.length > 0){
           var replybox = replyboxes[0];   
           comment.onclick = replybox.onclick;
         } //traditional reply model
       }
    }      
  }
  doStyleSheet();
}

function updateReplyClick(){
  if (localStorage.getItem(replyClickStorageTag) == 0){        
    var comments = document.getElementsByClassName('comment-body');
    for (comment in comments){
      comment.onclick = "";
    }
  }
  else{      
    var comments = document.getElementsByClassName('comment-body');
    if (localStorage.getItem(floatCommStorageTag) == 1){
       for (var i = 0; i < comments.length; i++){
         var comment = comments[i];               
         //floating reply model
         comment.addEventListener('click',replyTo);                 
       }
    }
    else {
      for (var i = 0; i < comments.length; i++){
         var comment = comments[i];      
         var replyboxes = comment.getElementsByClassName('comment-reply-link');
         if (replyboxes.length > 0){
           var replybox = replyboxes[0];   
           comment.onclick = replybox.onclick;
         } //traditional reply model
       }
    }
  }
}

function toggleDynamicLoad(){
  if (localStorage.getItem(dynamicLoadStorageTag) == 1){
    localStorage.setItem(dynamicLoadStorageTag,0);  
    document.getElementById('dynamic_load_button').value = "Dynamically load Comments";
    document.getElementById('dynamic_load_button').classList.remove("selected");
    dynamicLoadStop();
  }
  else{
    localStorage.setItem(dynamicLoadStorageTag,1);  
    document.getElementById('dynamic_load_button').value = "Don't load Comments";
    document.getElementById('dynamic_load_button').classList.add("selected");
    dynamicLoadInit();
  }
}

function toggleFloatComm(){
  if (localStorage.getItem(floatCommStorageTag) == 1){
    localStorage.setItem(floatCommStorageTag,0);  
    document.getElementById('float_comm_button').value = "Float Reply";
    document.getElementById('float_comm_button').classList.remove("selected");
	anchorReply();
  }
  else{
    localStorage.setItem(floatCommStorageTag,1);  
    document.getElementById('float_comm_button').value = "Don't float Reply";
    document.getElementById('float_comm_button').classList.add("selected");
    floatReply();
  }
}

function anchorReply(){
	var repcont = document.getElementById("respond");
	if (repcont.classList.contains("float")){
       repcont.classList.remove("float");
    }
}

function floatReply(){
	var repcont = document.getElementById("respond");
	repcont.classList.add("float");
    var textbox = document.getElementById("comment");
    textbox.cols = "";
    textbox.rows = "";
    var cancelButton = document.createElement("input");
    cancelButton.type = "button";
    cancelButton.id= "cancel_button";
    cancelButton.value = "Cancel Reply"
    cancelButton.addEventListener('click', cancelReply);
    cancelButton.classList.add("hidden");
    var repToButton = document.createElement("input");
    repToButton.type = "button";
    repToButton.id= "rep_to_button";
    repToButton.value = " ";
    repToButton.addEventListener('click', navToRep);
    repToButton.classList.add("hidden");
    var buttonp = repcont.querySelector(".form-submit");
    buttonp.appendChild(cancelButton);
    buttonp.appendChild(repToButton);
    
}

function replyTo(event){    
    var commnum = event.currentTarget.id.split("-")[1];
    var cp = document.getElementById("comment_parent");
    cp.value = commnum;
    var cancelButton = document.getElementById("cancel_button");
    cancelButton.classList.remove("hidden");
    var comment = document.getElementById("comment-" + commnum);
    var repToButton = document.getElementById("rep_to_button");
    repToButton.value = "Reply To " + comment.querySelector(".fn").innerHTML;
    repToButton.classList.remove("hidden");
}

function navToRep(){
    var cp = document.getElementById("comment_parent");
    var reply = document.getElementById("comment-"+cp.value);
    reply.scrollIntoView();
    window.scrollBy(0,-50);
}

function cancelReply(){    
    var cp = document.getElementById("comment_parent");
    cp.value = "0";
    var cancelButton = document.querySelector("#cancel_button");
    cancelButton.classList.add("hidden");   
    var repToButton = document.getElementById("rep_to_button");
    repToButton.classList.add("hidden");   
}

var timer = 0;
var ifrstr = "";
var pageframe = 0;
function dynamicLoadInit(){
   pageframe = document.createElement('iframe');
   var ifrurl = document.URL;
   ifrurl = ifrurl.split('/');
   for (var i = 3; i < ifrurl.length; i++){
     if (ifrurl[i].trim() == "" || ifrurl[i].startsWith("#")){
       break;
     }
     ifrstr = ifrstr + "/" + ifrurl[i];
   }
  pageframe.id = "comment_load_iframe";
  pageframe.style = "display: none";
  document.body.appendChild(pageframe);
  timer = setInterval(function(){dynamicLoadRefresh();},45000);
}

function dynamicLoadRefresh(){
  pageframe.src = ifrstr;
  pageframe.onload = function(){getNewComments();};
}

function getNewComments(){
  var cifr = document.getElementById("comment_load_iframe");
  var comments = cifr.contentDocument.querySelectorAll('.new-comment');
  if (comments.length > 0){
    var oldthr = document.querySelectorAll('.no-new-comment');
    for (var j = 0; j < oldthr.length; j++){
      oldthr[j].classList.remove('no-new-comment');
    }     
	  for (var i = 0; i < comments.length; i++){
		var parentComment = comments[i].parentNode.parentNode;
		var docParent = document.getElementById(parentComment.id);
		var docChildren = docParent.querySelectorAll(".children");
		if (docChildren.length > 0){
		  docChildren[0].appendChild(comments[i]);      
		}
		else{
		  var childrenNode = document.createElement("ul");
		  childrenNode.classList.add("children");
		  docParent.appendChild(childrenNode);
		  childrenNode.appendChild(comments[i]);
		}
	  }  
	  parseComments();
      updateReplyClick();
    linksInNewWindow();
	  var nextUnreadButton = document.getElementById("unread_button");
	  nextUnreadButton.value = document.querySelectorAll(".new-comment").length + " Unread Comments";
	  if (document.querySelectorAll(".new-comment").length == 1){
		nextUnreadButton.value = document.querySelectorAll(".new-comment").length + " Unread Comment";
	  }
  }
}

function dynamicLoadStop(){
  clearInterval(timer);
}

function setShowThreads(thisEle) {
  if (localStorage.getItem(hideThreadsStorageTag) == 1){
    thisEle.value = "Show Old Threads";
    document.getElementById("comment-wrap").classList.add("onlyNew");
  } 
  else{
    thisEle.value = "Hide Old Threads";
    document.getElementById("comment-wrap").classList.remove("onlyNew");
  }
}

function seekNextUnread(){
  var unreadList = document.querySelectorAll(".new-comment");
  if (unreadCount >= unreadList.length){
    unreadCount = 0;
  }
  if (unreadList.length > 0){
   unreadList[unreadCount].scrollIntoView();
   window.scrollBy(0,-50);
  }
  unreadCount+=1;
}

function toggleToolbar(){
  if ( document.getElementById("monocle_tb_container").classList.contains("mini") ) {
    document.getElementById("monocle_tb_container").classList.remove("mini");
    document.getElementById("respond").classList.remove("bottom");
    document.getElementById("toggle_button").value = "v";
  }
  else{
    document.getElementById("monocle_tb_container").classList.add("mini");
    document.getElementById("respond").classList.add("bottom");
    document.getElementById("toggle_button").value = "^";
  }
}

//collapse the reply button into the comment
function collapseReply(){
  var comments = document.querySelectorAll(".comment");
  for (var i = 0; i < comments.length; i++){    
    var replyButton = comments[i].querySelector(".reply-container");
    var replyLink = replyButton.querySelector("a");
    
  }
}

/*function replyToComment(){
   var replyLink = this.querySelector("a");   
}*/ //doesn't appear to be used

function killPreloadedCommentText(){
  var commentbox = document.getElementById("commentform");
  var commentText = commentbox.querySelector("label");
  commentText.parentNode.removeChild(commentText);
  var textArea = commentbox.querySelector("textarea");
  textArea.value = "";  
}

function linksInNewWindow(){
  var comments = document.getElementById("comment-wrap");
  var linklist = comments.querySelectorAll("a");
  for (var i = 0; i < linklist.length; i++){
    var link = linklist[i];
    link.target = "_blank";
  }
}

//Main

if (localStorage.getItem(expandMainColStorageTag) == 1){
  try{
    expandMainColumn();
  }
  catch(error){}
}


if (localStorage.getItem(timeZoneStorageTag) == 1){
  try{
    setTimeZone();
  }
  catch(error){}
}

try{
  if ( inputFormatter == "tinyMCE") {
      addTinyMCE();
     /* var replyButtons = document.getElementsByClassName('reply-container');
      for (button in replyButtons){
         button.addEventListener('click',resetTinyMCE);
      }*/
     var replydiv = document.getElementById("respond");
     replydiv.classList.add("tinyMCE");
  }
  else if (inputFormatter == "custom" ) {
      customFormatter();
  }

}
catch(error){console.log(error.name + ": " + error.message);}

try{
  createFooter();
}
catch(error){}
  
try{
    parseComments();
    doStyleSheet();
}
catch(error){}


if (fullCommentReply){
  try{
    collapseReply();
  }
  catch(error){}
}

try{
  linksInNewWindow();
}
catch(error){}
